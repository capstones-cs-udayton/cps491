# Source code to Markdown Tool

This is a tool for creating a report from source code files.

# Download/Installation

To use this tool, clone this repository or download the code2md.sh file in your machine, e.g., Google Cloud Shell.
From a shell, make it executable with the following command:

```bash
chmod a+x code2md.sh
```

# Usage

To use this tool, type the command as follows:

```bash
Usage: ./code2md.sh -d <directory> -e <extension> -l <language> -o <output_file>
```

## Examples:

### NodeJS

If your code is in Node.js:

```bash
./code2md.sh -d nodejs_src -e js -l nodejs -o sourcecode.md
```

If your code is in Python:

### Python
```bash
./code2md.sh -d python_src -e py -l python -o sourcecode.md
```
